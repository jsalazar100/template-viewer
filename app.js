const TemplateViewer = {
    data() {
        fetch('/data/template.json')
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                TemplateViewer.generateThumbGroups(data);
                TemplateViewer.generateLargeGroups(data);
            })
            .catch(function(err) {
                console.log('error: ' + err);
            });
    },
    generateThumbGroups(data) {
        let thumbContainer = document.getElementById('thumbnailRoot');
        let tgArray = [];
    
        for (let i = 0; i < data.length; i++) {        
            if (i % 4 === 0) {
                // create a container for each group of thumbnails
                let thumbGroup = document.createElement('div');
                thumbGroup.id = `tG${i}`;
                tgArray.push(`tG${i}`);
                thumbGroup.classList.add('thumbGroup');
                // hide all thumbnail containers except for 1st one
                if (i !== 0) {
                    thumbGroup.classList.add('group-hide');
                }
                thumbContainer.appendChild(thumbGroup);
            }
        }
    
        // add handlers for all clicks
        const handlerRoot = document.getElementById('thumbnailRoot');
        const previousButton = document.getElementById('previous');
        const nextButton = document.getElementById('next');
        let tgLength = tgArray.length;
        let currentIndex = 0;
        let showGroup = 0;
    
        handlerRoot.addEventListener('click', event => {
            const eT = event.target;
            // handler for thumbnail links
            if (eT.parentNode.classList.contains('thumbLink')) {
                // remove active class from all links
                document.querySelectorAll('.thumbLink').forEach(tLink => {
                    tLink.classList.remove('active')
                });
                // add active class to current selection
                eT.parentNode.classList.add('active');
     
                // hide all large image groups
                document.querySelectorAll('.largeGroup').forEach(largeImage => {
                    largeImage.classList.add('group-hide')
                });
                
                // show selected large image group  
                document.getElementById(eT.parentNode.title).classList.remove('group-hide');
                event.preventDefault();
            }
    
            // handler for prev/next carousel links
            if (eT.classList.contains('previous') || eT.classList.contains('next')) {
                // get index of visible group, then hide it
                tgArray.forEach((group, current) => {
                    let currentGroup = document.getElementById(group);
                    if (!currentGroup.classList.contains('group-hide')) {
                        currentIndex = current;
                        currentGroup.classList.add('group-hide');
                    } 
                });
    
                if (eT.classList.contains('next')) {
                    // calculate next group to display - not circular
                    showGroup = (currentIndex == tgLength - 1) ? tgLength - 1 : currentIndex + 1;
                    // enable prev button
                    previousButton.classList.remove('disabled');
    
                    if (currentIndex == tgLength - 2) {
                        // disable button
                        nextButton.classList.add('disabled');
                    }
                }
                else {
                    // calculate previous group to display - not circular
                    showGroup = (currentIndex == 0) ? 0 : currentIndex - 1;
                    // enable next button
                    nextButton.classList.remove('disabled');
                    
                    if (currentIndex == 1) {
                        // disable button
                        previousButton.classList.add('disabled');
                    }
    
                }
                
                // show prev or next thumbnail group  
                document.getElementById(tgArray[showGroup]).classList.remove('group-hide');
            }
            
            event.preventDefault();
        });
    
        // thumbgroups have been created, now fill them with children
        this.populateThumbGroups(data);
    },
    populateThumbGroups(data) {
        // cache empty thumbnail containers
        const thumbGroups = document.querySelectorAll('.thumbGroup');

        // loop thru containers and append children to each
        Array.from(thumbGroups).forEach((group, current) => {
            
            // determine which thumbnails to append to our container 
            let indexStart = Number(group.id.replace('tG', ''));
            let indexEnd = (current == thumbGroups.length - 1) ? data.length : indexStart + 4;
            // create thumbnails and append them
            for (let i = indexStart; i < indexEnd; i++) {
                let thumbLink = document.createElement('a');
                thumbLink.href = `#`;
                thumbLink.title = `${data[i].id}`;
                thumbLink.classList.add('thumbLink');
                if (i == 0) {
                    // add active class to first thumbnail
                    thumbLink.classList.add('active');
                }
                thumbLink.innerHTML = `<img src="/images/thumbnails/${data[i].thumbnail}" alt="${data[i].id}" width="145" height="121" /><span>${data[i].id}</span>`;
                // add children to group container
                document.getElementById(group.id).appendChild(thumbLink);
            }

        });
    },
    generateLargeGroups(data) {
        let largeContainer = document.getElementById('largeRoot');

        for (let i = 0; i < data.length; i++) {
            
            // greate group and add id to group and store in array
            let largeGroup = document.createElement('div');
                largeGroup.id = data[i].id;
                largeGroup.classList.add('largeGroup');
                if (i !== 0) {
                    largeGroup.classList.add('group-hide');
                }
                largeGroup.innerHTML = `
                <img src="images/large/${data[i].image}" alt="${data[i].title}" />
                <div class="details">
                    <p><strong>Title</strong> ${data[i].title}</p>
                    <p><strong>Description</strong>  ${data[i].description}</p>
                    <p><strong>Cost</strong> ${data[i].cost}</p>
                    <p><strong>ID #</strong> ${data[i].id}</p>
                    <p><strong>Thumbnail File</strong> ${data[i].thumbnail}</p>
                    <p><strong>Large Image File</strong> ${data[i].image}</p>
                </div>
                `;
    
            largeContainer.appendChild(largeGroup);
    
        }
    },
    render() {
        this.data();
    }

    
};

TemplateViewer.render();